#include <stdio.h>
int main ()
{

	int A=10;
	int B=15;
	
  printf("A = 10\nB = 15\n\n");
	
	
	printf("Bitwise AND (&) \n");
	printf("Output = %d \n\n", A&B);
	
	printf("Bitwise XOR (^) \n");
	printf("Output = %d \n\n", A^B);
	
	printf("Bitwise Complement (~) \n");
	printf("Output of A = %d \n", ~10);
	printf("Output of B = %d \n\n", ~15);
	
	printf("Shift Left (<<) \n");
	
		printf("A before left shifting: %d\n", A);
		printf("A after left shifting: %d\n\n", A<<1);
		
		printf("B before left shifting: %d\n", B);
		printf("B after left shifting: %d\n\n", B<<1);
		
	printf("Shift Right (>>) \n");
	
		printf("A before right shifting: %d\n", A);
		printf("A after right shifting: %d\n\n", A>>1);
		
		printf("B before right shifting: %d\n", B);
		printf("B after right shifting: %d\n\n", B>>1);
	
	return 0;
	
	}
